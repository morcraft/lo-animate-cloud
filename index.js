const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Some arguments are expected')

    if(!(args.elements instanceof NodeList))
        throw new Error('Invalid elements')
    
    _.forEach(args.elements, function(element, key){
        const delay = (Math.ceil(Math.random() * 120) * -1) + 's'
        const style = {
            '-webkit-animation-delay': delay,
            'animation-delay': delay
        }
        
        element.classList.add('cloud-animation')
        _.merge(element.style, style)
    })
}
